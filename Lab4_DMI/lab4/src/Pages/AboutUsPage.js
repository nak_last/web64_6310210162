import AboutUs from '../components/AboutUs';

function AboutUsPage() {
    return (
        <div align='center'>
            <table>
                <tr>
                    <th>
                        <h1> CREATOR </h1>
                        <hr />
                    </th>
                </tr>
                <tr>
                    <th>
                        <AboutUs name="Nak_last" 
                                contact="ติดต่อได้ที่ Discord ตั้งแต่ 10:00 am. - 00:00 pm."
                                address="หา Nak_Last ใต้หวรรค์บนดิน"/>
                    </th>
                </tr>
            </table>
        </div>
    );
}export default AboutUsPage;