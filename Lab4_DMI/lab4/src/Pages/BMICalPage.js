import BMIResult from "../components/ฺBMIResult";
import { useState } from "react";

function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmi, setBmiResult ] = useState("");
    const [ translate, setTranslateResult ] = useState("");

    const [ height, setHeight ] = useState("");
    const [ weight, setWeight ] = useState("");

    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);

        setBmiResult( bmi );

        if(bmi > 25) {
            setTranslateResult("อิอ้วน")
        }else {
            setTranslateResult("ไอผอม")
        }
    }

    return (
        <div align='center'>
            <h1> Welcome to BMI-calculator </h1>
            <hr />

                คุณชื่อ: <input type="text"
                            value={ name } 
                            onChange={ (e) => { setName (e.target.value); } } /> <br />
                ส่วนสูง: <input type="text" 
                            value={ height } 
                            onChange={ (e) => { setHeight (e.target.value); } } /> <br />
                น้ำหนัก: <input type="text" 
                            value={ weight } 
                            onChange={ (e) => { setWeight (e.target.value); } } /> <br />
                <br />
                <button onClick={ () => { calculateBMI() } }> Calculate </button>

            {   bmi != 0 && 
                <div>
                    <hr />
                        ผลการคำนวณ
                        <BMIResult name={ name } 
                                bmi={ bmi }
                                result={ translate } />
                </div>
            }
        </div>
    );
}export default BMICalPage;