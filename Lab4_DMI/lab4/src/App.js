import AboutUsPage from './Pages/AboutUsPage';
import BMICalPage from './Pages/BMICalPage';
import LuckyNumberPage from './Pages/LuckyNumberPage';
import Header from './components/Header';

import AboutUs from './components/AboutUs';
import BMIResult from './components/ฺBMIResult';
import LuckyNumber from './components/LuckyNumber';

import './App.css';
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div>
      <Header />
      <Routes>
        <Route path="about" element={ <AboutUsPage /> } />
        <Route path="/" element={ <BMICalPage /> } />
        <Route path="luck" element={ <LuckyNumberPage />} />
      </Routes>
    </div>
  );

}export default App;
