import AboutUsPage from "../Pages/AboutUsPage";

function AboutUs(props) {

    return (
        <div>
            <h2> Create by {props.name}</h2>
            <h3> Content: {props.contact} </h3>
            <h3> Address: {props.address} </h3>
        </div>
    );
}export default AboutUs;