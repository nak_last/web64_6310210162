import './Header.css';
import { Link } from 'react-router-dom';

function Header() {
    return (
        <div id="AppBar">
            <h1 id='text'> 
                BMI Menu 
                &nbsp;
                <button> <Link to="/"> BMI-Calculator </Link> </button>
                &nbsp;
                <button> <Link to="luck"> Lucky-Number-for-you </Link> </button>
                &nbsp;
                <button> <Link to="about"> AboutUs-Creator </Link> </button>
            </h1>
        </div>
    );
}export default Header