import { Box, Toolbar, Typography, AppBar, Card, IconButton } from '@mui/material';
import CardContent from '@mui/material/CardContent';
import CloudIcon from '@mui/icons-material/Cloud';

import { useEffect, useState } from 'react';
import axios from 'axios';
import ReactWeather, { useOpenWeather } from 'react-open-weather';

import YouTube from 'react-youtube';

import './App.css';

function App() {
  const [ temperature, settemperature ] = useState();

  const WeatherAPIBaseURL = 'http://api.openweathermap.org/data/2.5/weather?';
  const city = '7.199&lon=100.595';
  // openWeatherAPIkey: d138b3edd3d287e10dd692507d63978c
  const APIkey = 'd138b3edd3d287e10dd692507d63978c';

  const { data, isLoading, errorMessage } = useOpenWeather({
    key: 'd138b3edd3d287e10dd692507d63978c',
    lat: '7.199',
    lon: '100.595',
    lang: 'en',
    unit: 'metric'
  });

  useEffect( () => {
    settemperature("-----");

    axios.get(WeatherAPIBaseURL+'lat='+city+'&appid='+APIkey).then ((response) => {
      let data = response.data;
      let temp = data.main.temp - 273;
      settemperature(temp.toFixed(2));
    })
  }
  , [] );

  return (
    <Box sx={{ flexGrow: 1, width: '100%' }}>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant='h5'>
            <CloudIcon /> &nbsp; Weather App
          </Typography>
        </Toolbar>
      </AppBar>

      <Box sx={{ justifyContent: 'center', marginTop: '20px', width: '100%', display: 'flex'}}>
        <Typography variant='h4'>
          Songkhla temperature today
        </Typography>
      </Box>

      <Box sx={{ justifyContent: 'left', marginTop: '20px', width: '100%', display: 'flex' }}>
        <Card sx={{ minWidth: 275 }}>
              <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary">
                  Songkhla temperature today is
                </Typography>
                <br />
                <Typography variant="h4" component="div">
                  { temperature } &nbsp;  ํC
                </Typography>
                <br />
              </CardContent>
            </Card>
            <Box sx={{ justifyContent: 'right', marginTop: '20px', width: '100%', display: 'flex' }}>
              <YouTube videoId='u-GJBlpLoDE' />
            </Box>
      </Box>

      <Box sx={{ justifyContent: 'center', marginTop: '20px' }}>
        <ReactWeather
          isLoading={isLoading}
          errorMessage={errorMessage}
          data={data}
          lang="en"
          locationLabel="Songkhla"
          unitsLabels={{ temperature: 'ํC', windSpeed: 'Km/h' }}
          showForecast
        />
      </Box>
      <Box mt={2}>
        <AppBar position='static' sx={{ top: 'auto', bottom: 0 }}>
          <Toolbar>
            Power By CS
          </Toolbar>
        </AppBar>
      </Box>
    </Box>
  );
}

export default App;