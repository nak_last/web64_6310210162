import Header from './components/Header.js';
import Body from './components/Body.js';
import Footer from './components/Footer.js';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">

      <div>
        
        <Header />
        <Body />
        <Footer />

      </div>
      
      </header>
    </div>
  );
}

export default App;
