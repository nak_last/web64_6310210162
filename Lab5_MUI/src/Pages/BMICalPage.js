import BMIResult from "../components/ฺBMIResult";
import { useState } from "react";
import { Button, Typography, Box, Container } from "@mui/material";
import Grid from "@mui/material/Grid";

function BMICalPage() {

    const [ name, setName ] = useState("");
    const [ bmi, setBmiResult ] = useState("");
    const [ translate, setTranslateResult ] = useState("");

    const [ height, setHeight ] = useState("");
    const [ weight, setWeight ] = useState("");

    function calculateBMI() {
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);

        setBmiResult( bmi );

        if(bmi > 25) {
            setTranslateResult("อิอ้วน")
        }else {
            setTranslateResult("ไอผอม")
        }
    }

    return (
        <Container sx={{ width: "80%", textAlign: "center" }}>
            <Grid container spacing={2} sx={{ marginTop: '10px' }}>
                <Grid item xs={12}>
                    <Box sx={{ textAlign: "center"}}>
                        <Typography variant="h5"> 
                            Welcome to BMI-calculator 
                        </Typography>
                    </Box>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign: "left"}}>
                        คุณชื่อ: <input type="text"
                                    value={ name } 
                                    onChange={ (e) => { setName (e.target.value); } } /> 
                                    <br />
                                    <br />
                        ส่วนสูง: <input type="text" 
                                    value={ height } 
                                    onChange={ (e) => { setHeight (e.target.value); } } /> 
                                    <br />
                                    <br />
                        น้ำหนัก: <input type="text" 
                                    value={ weight } 
                                    onChange={ (e) => { setWeight (e.target.value); } } /> 
                                    <br />
                                    <br />
                        <br />
                        <Button variant="contained" disableElevation onClick={ () => { calculateBMI() } }>Calculate</Button>
                    </Box>
                </Grid>
                <Grid item xs={4}>
                    <Box sx={{ textAlign: "center" }}>
                        {
                            bmi != 0 && 
                           <div>
                                ผลการคำนวณ
                               <BMIResult name={ name } 
                                          bmi={ bmi }
                                          result={ translate } />
                            </div>
                        }
                    </Box>
                </Grid>
            </Grid>
        </Container>
    );
}export default BMICalPage;

{/* <div align='center'>
            <h1> Welcome to BMI-calculator </h1>
            <hr />

                คุณชื่อ: <input type="text"
                            value={ name } 
                            onChange={ (e) => { setName (e.target.value); } } /> <br />
                ส่วนสูง: <input type="text" 
                            value={ height } 
                            onChange={ (e) => { setHeight (e.target.value); } } /> <br />
                น้ำหนัก: <input type="text" 
                            value={ weight } 
                            onChange={ (e) => { setWeight (e.target.value); } } /> <br />
                <br />
                {/* <button onClick={ () => { calculateBMI() } }> Calculate </button> */}
        //         <Button variant="contained" disableElevation onClick={ () => { calculateBMI() } }>Calculate</Button>

        //     {   bmi != 0 && 
        //         <div>
        //             <hr />
        //                 ผลการคำนวณ
        //                 <BMIResult name={ name } 
        //                         bmi={ bmi }
        //                         result={ translate } />
        //         </div>
        //     }
        // </div> */}