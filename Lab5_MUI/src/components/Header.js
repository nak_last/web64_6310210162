import './Header.css';
import { Link } from 'react-router-dom';
import { AppBar, IconButton, Toolbar, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import MenuIcon from '@mui/icons-material/Menu';

function Header() {
    return (
        <Box sx={{ flexGrow: 1}}>
            <AppBar position='static'>
                <Toolbar>
                <Typography variant='h4'> 
                BMI Menu 
                &nbsp;
                <button> <Link to="/"> BMI-Calculator </Link> </button>
                &nbsp;
                <button> <Link to="luck"> Lucky-Number-for-you </Link> </button>
                &nbsp;
                <button> <Link to="about"> AboutUs-Creator </Link> </button>
                </Typography>
                </Toolbar>
            </AppBar>
        </Box>
    );
}export default Header
