import AboutUsPage from "../Pages/AboutUsPage";
import { Paper, Box } from "@mui/material";


function AboutUs(props) {

    return (
        <Box sx={{ width: "60%" }}>
            <Paper elevation={3}>
                <h2> Create by {props.name}</h2>
                <h3> Content: {props.contact} </h3>
                <h3> Address: {props.address} </h3>
            </Paper>
        </Box>
    );
}export default AboutUs;