const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/BMI', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if ( !isNaN(weight) && !isNaN(height)) {
        let BMI = weight / (height * height)
        result = {
            "status" : 200,
            "BMI" : BMI,
        }
    }else{
        result ={
            "status" : 400,
            "message" : "Weight or Height is not a number"
        }
    }

    res.send(JSON.stringify(result))
})

app.get('/hello', (req, res) => {
    res.send('Sawadee ' + req.query.name)
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
}) 