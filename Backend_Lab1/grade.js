const express = require('express')
const app = express()
const port = 4000

app.post('/grade', (req, res) => {
    let name = (req.query.name)
    let score = parseInt(req.query.score)
    var result = {}

    if ( !isNaN(score) && ((score >= 0) && (score <= 100))) {
        let grade

        if(score >= 80){
            grade ="A"
        }else if(score >= 70){
            grade ="B"
        }else if(score >= 50){
            grade ="C"
        }else if(score >= 40){
            grade ="D"
        }else {
            grade ="E"
        }

        result = {
            "status" : 200,
            "name" : name,
            "grade" : grade,
        }
    }else{
        result = {
            "status" : 400,
            "mesage" : "name is not character or score is not number",
        }
    }

    res.send(JSON.stringify(result))
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})