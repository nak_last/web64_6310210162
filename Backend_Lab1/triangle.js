const express = require('express')
const app = express()
const port = 4000

app.get('/triangleArea', (req, res) => {
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if ( !isNaN(height) && !isNaN(base)) {
        let area = height * base / 2

        result = {
            "status" : 200,
            "area" : area,
        }
    }else{
        result = {
            "status" : 400,
            "mesage" : "height or base is not number",
        }
    }

    res.send(JSON.stringify(result))
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})